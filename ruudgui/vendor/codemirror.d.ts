declare interface CodeMirrorConfiguration {
    electricChars: boolean;
    indentUnit: number;
    lineNumbers: boolean;
    mode: string;
    value: string;
}

declare function CodeMirror(
    container: HTMLElement,
    configuration: CodeMirrorConfiguration,
): void;
