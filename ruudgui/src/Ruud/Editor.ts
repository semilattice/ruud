namespace Ruud.Editor {
    export function initialize(container: HTMLElement): void {
        CodeMirror(container, {
            electricChars: false,
            indentUnit: 4,
            lineNumbers: true,
            mode: "text/x-csrc",
            value: "int main() {\n    return 0;\n}\n",
        });
    }
}
