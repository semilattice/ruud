namespace Ruud.Example {
    export type Tree =
        { tag: "Dir", contents: { [key: string]: Tree } } |
        { tag: "File" };
}
