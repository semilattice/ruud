{ stdenv, nodePackages, fetchzip }:
let
  codemirror = fetchzip {
    url = https://codemirror.net/codemirror-5.40.2.zip;
    sha256 = "099dp0jmlc67iz1v993yypq93i597rzkz80734awifg78pic33ba";
  };
in
  stdenv.mkDerivation {
    name = "ruudgui";
    src = ./.;
    buildInputs = [
      codemirror
      nodePackages.typescript
    ];
    buildPhase = ''
      tsc
    '';
    installPhase = ''
      mkdir "$out"

      mkdir "$out"/codemirror
      cp -R '${codemirror}'/lib '${codemirror}'/mode "$out"/codemirror

      cp ruud.js "$out"/ruud.js

      cp src/index.html "$out"
    '';
  }
