{ pkgs ? import ./nix/nixpkgs.nix }:
rec {
  ruudgui = pkgs.callPackage ./ruudgui {};
  ruudrun = pkgs.haskellPackages.callPackage ./ruudrun {};
  run-in-vm-example = pkgs.callPackage ./docs/run-in-vm-example.nix {};
  tests = {
    acceptance = pkgs.callPackage ./tests/acceptance {
      ruudrun = ruudrun;
    };
  };
}
