{vmTools, runCommand, hello}:
let
  cmd = runCommand "drv-name" {buildInputs = [ hello ];} "hello";
in
  vmTools.runInLinuxVM cmd
