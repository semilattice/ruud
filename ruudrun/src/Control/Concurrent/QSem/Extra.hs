module Control.Concurrent.QSem.Extra
  ( withQSem
  ) where

import Control.Concurrent.QSem (QSem, signalQSem, waitQSem)
import Control.Monad.Catch (MonadMask, bracket_)
import Control.Monad.IO.Class (MonadIO, liftIO)

withQSem :: (MonadMask m, MonadIO m) => QSem -> m a -> m a
withQSem sem = bracket_ (liftIO (waitQSem sem))
                        (liftIO (signalQSem sem))
