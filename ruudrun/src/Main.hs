{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Main
  ( main
  ) where

import Control.Applicative ((<**>))
import Control.Concurrent.QSem (QSem, newQSem)
import Control.Monad.Catch (MonadMask, handle)
import Control.Monad.Error.Class (throwError)
import Control.Monad.Free (Free, foldFree)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Proxy (Proxy (..))
import Servant.API ((:>))

import qualified Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import qualified Options.Applicative as Options
import qualified Servant.Server as Server

import Ruud.Config (Config (..), configOptions)
import Ruud.Example (RunExample)
import Ruud.Example.Api (ExamplesApi, examplesServer)
import Ruud.Run (NoSuchExample (..))

import qualified Ruud.Example.Essence as Example.Essence
import qualified Ruud.Example.Scheduling as Example.Scheduling

main :: IO ()
main = do
  let argumentParser :: Options.ParserInfo Config
      argumentParser = Options.info (configOptions <**> Options.helper)
                                    Options.fullDesc
  config <- Options.execParser argumentParser

  sem <- newQSem (fromIntegral $ configConcurrency config)

  let int :: Free RunExample a -> Server.Handler a
      api :: Proxy ("examples" :> ExamplesApi)
      app :: Wai.Application
      int = handle toServantError . liftIO . interpret config sem
      api = Proxy
      app = Server.serve api . Server.enter (Server.NT int) $
              examplesServer

  Warp.run (fromIntegral (configApiPort config)) app

interpret :: (MonadMask m, MonadIO m) => Config -> QSem -> Free RunExample a -> m a
interpret config sem =
  foldFree (
    Example.Scheduling.interpretRunExample sem (
      Example.Essence.interpretRunExample config
    )
  )

toServantError :: NoSuchExample -> Server.Handler a
toServantError NoSuchExample = throwError Server.err404
