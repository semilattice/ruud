-- | I/O at compile time.
module System.IO.Static
  ( readFile
  ) where

import Prelude hiding (readFile)

import Language.Haskell.TH (Q, Exp (..), Lit (..), runIO)

import qualified Prelude

-- | Read a file, at compile time.
readFile :: FilePath -> Q Exp
readFile = fmap (LitE . StringL) . runIO . Prelude.readFile
