module Data.Maybe.Extra
  ( note
  ) where

note :: e -> Maybe a -> Either e a
note = flip maybe Right . Left
