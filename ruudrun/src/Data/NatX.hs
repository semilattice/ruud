{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}

module Data.NatX
  ( type (~>)
  ) where

type (~>) f g = forall a. f a -> g a
