{ pkgs ? import <nixpkgs> {}, command, stdin }:
let
  command' = ''
    printf '%s' "$stdin" | (${command}) 1>> "$out"/stdout 2>> "$out"/stderr
  '';
  commandDrv = pkgs.runCommand "ruudrun-run-command" {
    buildInputs = [ (pkgs.callPackage ./. {}) ];
    stdin = stdin;
  } command';
in
  pkgs.vmTools.runInLinuxVM commandDrv
