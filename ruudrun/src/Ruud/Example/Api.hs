{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Ruud.Example.Api
  ( -- * APIs
    ExamplesApi
  , ExamplesExampleApi
  , ExamplesExampleRunApi

    -- * Servers
  , examplesServer
  , examplesExampleServer
  , examplesExampleRunServer

    -- * Requests
  , ExamplesExampleRunRequest

    -- * Responses
  , ExamplesExampleRunResponse
  ) where

import Control.Monad.Free.Class (MonadFree)
import Data.Aeson (FromJSON, ToJSON, (.:), (.=))
import Data.ByteString (ByteString)
import Data.Coerce (coerce)
import Servant.API ((:>))
import Servant.Server (ServerT)
import System.Exit (ExitCode (..))

import qualified Data.Aeson as AE
import qualified Data.ByteString.Base64 as BS.B64
import qualified Data.ByteString.Char8 as BS.C8
import qualified Servant.API as Api

import Ruud.Example (ExampleId, RunExample, runExample)

import qualified Ruud.Run as Run

type ExamplesApi =
  Api.Capture "exampleId" ExampleId :> ExamplesExampleApi

type ExamplesExampleApi =
  "run" :> ExamplesExampleRunApi

type ExamplesExampleRunApi =
  Api.ReqBody '[Api.JSON] ExamplesExampleRunRequest
  :> Api.Post '[Api.JSON] ExamplesExampleRunResponse

newtype ExamplesExampleRunRequest =
  ExamplesExampleRunRequest ByteString

newtype ExamplesExampleRunResponse =
  ExamplesExampleRunResponse Run.Output

examplesServer :: MonadFree RunExample m => ServerT ExamplesApi m
examplesServer = examplesExampleServer

examplesExampleServer :: MonadFree RunExample m => ExampleId
                      -> ServerT ExamplesExampleApi m
examplesExampleServer exampleId =
  examplesExampleRunServer exampleId

examplesExampleRunServer :: MonadFree RunExample m => ExampleId
                         -> ServerT ExamplesExampleRunApi m
examplesExampleRunServer exampleId command = fmap ExamplesExampleRunResponse $
  runExample exampleId (coerce command)

instance FromJSON ExamplesExampleRunRequest where
  parseJSON = AE.withObject "ExamplesExampleRunRequest" $ \obj -> do
    command <- either fail pure . BS.B64.decode . BS.C8.pack =<< obj .: "command"
    pure $ ExamplesExampleRunRequest command

instance ToJSON ExamplesExampleRunResponse where
  toJSON (ExamplesExampleRunResponse output) =
    AE.object [ "stdout" .= BS.C8.unpack (BS.B64.encode (Run.outputStdout output))
              , "stderr" .= BS.C8.unpack (BS.B64.encode (Run.outputStderr output))
              , "exitCode" .= case Run.outputStatus output of
                                { ExitSuccess   -> 0
                                ; ExitFailure n -> n } ]
