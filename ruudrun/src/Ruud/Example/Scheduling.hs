{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}

-- | The scheduling aspect of the Run example use case.
module Ruud.Example.Scheduling
  ( interpretRunExample
  ) where

import Control.Concurrent.QSem (QSem)
import Control.Monad.Catch (MonadMask)
import Control.Monad.IO.Class (MonadIO)

import Control.Concurrent.QSem.Extra (withQSem)
import Data.NatX (type (~>))
import Ruud.Example (RunExample)

interpretRunExample :: (MonadMask m, MonadIO m)
                    => QSem -> (RunExample ~> m)
                    -> RunExample ~> m
interpretRunExample = (.) . withQSem
