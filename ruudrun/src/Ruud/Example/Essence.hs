-- | The essence of the Run example use case.
module Ruud.Example.Essence
  ( interpretRunExample
  ) where

import Control.Monad.Catch (MonadCatch)
import Control.Monad.IO.Class (MonadIO)
import Data.Semigroup ((<>))

import qualified Data.Text as Text

import Ruud.Config (Config, configExamples)
import Ruud.Example (RunExample (..), unExampleId)
import Ruud.Run (Input (..), run)

interpretRunExample :: (MonadCatch m, MonadIO m) => Config -> RunExample a -> m a
interpretRunExample config (RunExample next (unExampleId -> exampleId) command) =
  next <$> run input
  where
  examplePath = configExamples config <> "/" <> Text.unpack exampleId
  input = Input { inputPath = examplePath, inputStdin = "", inputCommand = command }
