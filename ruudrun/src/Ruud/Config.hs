{-# LANGUAGE ApplicativeDo #-}

-- | ruudrun configuration.
module Ruud.Config
  ( Config (..)
  , configOptions
  ) where

import Data.Semigroup ((<>))
import Data.Word (Word16)

import qualified Options.Applicative as Options

-- | ruudrun configuration.
data Config =
  Config
    { configApiPort     :: Word16
    , configExamples    :: FilePath
    , configConcurrency :: Word16 }
  deriving stock (Eq, Show)

configOptions :: Options.Parser Config
configOptions = do
  apiPort     <- Options.option Options.auto $
                   Options.long "api-port" <>
                   Options.metavar "PORT" <>
                   Options.help "Port on which to listen for API connections"
  examples    <- Options.strOption $
                   Options.long "examples" <>
                   Options.metavar "PATH" <>
                   Options.help "Path at which to find and store examples"
  concurrency <- Options.option Options.auto $
                   Options.long "concurrency" <>
                   Options.metavar "WORD" <>
                   Options.help "How many examples may run concurrently"
  pure Config { configApiPort = apiPort
              , configExamples = examples
              , configConcurrency = concurrency }
