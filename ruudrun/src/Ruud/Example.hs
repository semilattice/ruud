module Ruud.Example
  ( -- * Identification
    ExampleId
  , mkExampleId
  , unExampleId

    -- * Use cases
  , RunExample (..)
  , runExample
  ) where

import Control.Monad ((<=<), guard)
import Control.Monad.Free.Class (MonadFree, liftF)
import Data.ByteString (ByteString)
import Data.Semigroup ((<>))
import Data.Text (Text)
import Web.HttpApiData (FromHttpApiData, parseUrlPiece)

import qualified Data.Text as Text

import Data.Maybe.Extra (note)

import qualified Ruud.Run as Run

--------------------------------------------------------------------------------
-- Identification

newtype ExampleId = ExampleId Text
  deriving stock (Eq, Ord, Show)

mkExampleId :: Text -> Maybe ExampleId
mkExampleId raw = do
  guard $ not (Text.null raw)
  guard $ Text.all (`elem` ['a' .. 'z'] <> ['A' .. 'Z']) raw
  guard $ Text.length raw < 16
  pure $ ExampleId raw

unExampleId :: ExampleId -> Text
unExampleId (ExampleId raw) = raw

instance FromHttpApiData ExampleId where
  parseUrlPiece = note "invalid" . mkExampleId <=< parseUrlPiece

--------------------------------------------------------------------------------
-- Use cases

data RunExample a =
  RunExample (Run.Output -> a) ExampleId ByteString
  deriving stock (Functor)

runExample :: MonadFree RunExample m => ExampleId -> ByteString -> m Run.Output
runExample = (liftF .) . RunExample id
