{-# LANGUAGE TemplateHaskell #-}

-- | Run a project in a virtual machine and report the results.
module Ruud.Run
  ( Input (..)
  , Output (..)
  , NoSuchExample (..)
  , run
  , runCreateProcess
  , runCommand
  ) where

import Control.Exception (Exception)
import Control.Monad.Catch (MonadCatch, MonadThrow, handle, throwM)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.ByteString (ByteString)
import Data.Semigroup ((<>))
import System.Exit (ExitCode)
import System.IO.Error (isDoesNotExistError)
import System.IO.Temp (withSystemTempDirectory)
import System.Process (CreateProcess (CreateProcess))

import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BS.C8
import qualified System.Process as Proc

import qualified System.IO.Static as Static

-- | Template for the temporary working directory of the master.
masterWorkingDirTemplate :: String
masterWorkingDirTemplate = "ruudrun-run"

-- | A description of what to run.
data Input =
  Input
    { inputPath        :: FilePath
    , inputCommand     :: ByteString
    , inputStdin       :: ByteString }
  deriving stock (Eq, Show)

-- | The output of a run.
data Output =
  Output -- TODO: Record the build logs.
    { outputStdout :: ByteString
    , outputStderr :: ByteString
    , outputStatus :: ExitCode }
  deriving stock (Eq, Show)

-- | Exception when example does not exist.
data NoSuchExample = NoSuchExample
  deriving stock (Eq, Show)
  deriving anyclass (Exception)

-- | Run a project in a virtual machine and report the results.
run :: (MonadCatch m, MonadIO m) => Input -> m Output
run input = handle runRethrow . liftIO $
  withSystemTempDirectory masterWorkingDirTemplate $ \mwdir ->
  Proc.withCreateProcess (runCreateProcess mwdir input) $ \_ _ _ pid -> do
    status <- Proc.waitForProcess pid
    -- FIXME: The user could set up their command in such a way that these end
    -- FIXME: up as symlinks to e.g. /etc/passwd. Fix this somehow.
    stdoutData <- BS.readFile (mwdir <> "/result/stdout")
    stderrData <- BS.readFile (mwdir <> "/result/stderr")
    pure Output { outputStdout = stdoutData
                , outputStderr = stderrData
                , outputStatus = status }

-- | Rethrow exceptions from running.
runRethrow :: MonadThrow m => IOError -> m a
runRethrow e | isDoesNotExistError e = throwM NoSuchExample
             | otherwise = throwM e

-- | The process that 'run' will create.
runCreateProcess :: FilePath -> Input -> CreateProcess
runCreateProcess mwdir input =
  CreateProcess
    { Proc.cmdspec            = uncurry Proc.RawCommand (runCommand mwdir input)
    , Proc.cwd                = Just (inputPath input)
    , Proc.env                = inherit
    , Proc.std_in             = Proc.NoStream
    , Proc.std_out            = Proc.NoStream
    , Proc.std_err            = Proc.NoStream
    , Proc.close_fds          = True
    , Proc.create_group       = False
    , Proc.delegate_ctlc      = False
    , Proc.detach_console     = windowsOnly
    , Proc.create_new_console = windowsOnly
    , Proc.new_session        = False
    , Proc.child_group        = inherit -- TODO: Review if this should be overridden.
    , Proc.child_user         = inherit -- TODO: Review if this should be overridden.
    , Proc.use_process_jobs   = windowsOnly }
  where
  -- | Inherit this property from the parent process (ruudrun).
  inherit :: Maybe a
  inherit = Nothing

  -- | This property applies only on Windows. Windows is not supported by
  -- ruudrun so this can be an arbitrary value.
  windowsOnly :: Bool
  windowsOnly = False

-- | The command that 'run' will run.
runCommand :: FilePath -> Input -> (String, [String])
runCommand mwdir input = (nixBuild, arguments)
  where
  nixBuild :: String
  nixBuild = "nix-build"

  arguments :: [String]
  arguments =
    -- TODO: Enable restricted evaluation of Nix expressions.
    -- TODO: When starting ruudrun, ensure that the system locale is C.
    [ "--out-link", mwdir <> "/result"
    , "--expr", $(Static.readFile "src/Ruud/Run.nix")
    , "--argstr", "command", BS.C8.unpack (inputCommand input)
    , "--argstr", "stdin", BS.C8.unpack (inputStdin input) ]
