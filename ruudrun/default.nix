{ mkDerivation, stdenv, aeson, base, base64-bytestring, bytestring, containers
, exceptions, free, http-api-data, mtl, optparse-applicative, process, servant
, servant-server, template-haskell, temporary, text, wai, warp }:
mkDerivation {
  pname = "ruudrun";
  version = "0.0.0.0";
  src = ./.;
  isExecutable = true;
  executableHaskellDepends = [
    aeson
    base
    base64-bytestring
    bytestring
    containers
    exceptions
    free
    http-api-data
    mtl
    optparse-applicative
    process
    servant
    servant-server
    template-haskell
    temporary
    text
    wai
    warp
  ];
  license = stdenv.lib.licenses.bsd3;
}
