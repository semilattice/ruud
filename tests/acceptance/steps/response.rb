Then(/^the server responds with status "([^"]*)"$/) do |expected_status|
    actual_status = "#{@response.code} #{@response.message}"
    unless actual_status == expected_status
        raise "Expected status: #{expected_status.inspect}, " +
              "actual status: #{actual_status.inspect}"
    end
end
