require 'base64'
require 'json'
require 'net/http'

Given(/^the command "([^"]*)"$/) do |command|
    @command = command
end

When(/^the example is run$/) do
    Ruudrun.daemon do
        Net::HTTP.start('localhost', Ruudrun::API_PORT) do |http|
            request = Net::HTTP::Post.new("/examples/#{@example}/run")
            request['Content-Type'] = 'application/json'
            request.body = JSON.generate({
                'command' => Base64.strict_encode64(@command),
            })

            @response = http.request(request)
            begin
                @response_data = JSON.parse(@response.body)
            rescue JSON::ParserError
            end
        end
    end
end

Then(/^the server responds with stdout "([^"]*)"$/) do |expected_stdout|
    actual_stdout = Base64.decode64(@response_data["stdout"]).strip
    unless actual_stdout == expected_stdout
        raise "Expected stdout: #{expected_stdout.inspect}, " +
              "actual stdout: #{actual_stdout.inspect}"
    end
end

Then(/^the server responds with stderr "([^"]*)"$/) do |expected_stderr|
    actual_stderr = Base64.decode64(@response_data["stderr"]).strip
    unless actual_stderr == expected_stderr
        raise "Expected stderr: #{expected_stderr.inspect}, " +
              "actual stderr: #{actual_stderr.inspect}"
    end
end

Then(/^the server responds with exit code "([^"]*)"$/) do |expected_exit_code|
    actual_exit_code = @response_data["exitCode"]
    expected_exit_code = expected_exit_code.to_i
    unless actual_exit_code == expected_exit_code
        raise "Expected exit code: #{expected_exit_code.inspect}, " +
              "actual exit code: #{actual_exit_code.inspect}"
    end
end
