module Ruudrun
    API_PORT    = ENV.fetch('RUUDRUN_API_PORT', '8080').to_i
    EXAMPLES    = 'examples'
    CONCURRENCY = ENV.fetch('RUUDRUN_CONCURRENCY', '4').to_i

    def self.daemon
        pid = Process.spawn(
            'ruudrun',
            '--api-port',    API_PORT.to_s,
            '--examples',    EXAMPLES,
            '--concurrency', CONCURRENCY.to_s,
        )
        begin
            sleep 0.5
            yield
        ensure
            Process.kill :TERM, pid
            Process.wait pid
        end
    end
end
