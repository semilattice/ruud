{ stdenv, writeTextFile
, cucumber, nix, ruudrun }:
let
  ruudacceptance =
    stdenv.mkDerivation {
      name = "ruudacceptance";
      src = ./.;
      buildPhase = ''
        cat <<'EOF' >> 'ruudacceptance'
        #!/bin/sh
          ln -s '${./../examples}' 'examples'
          trap 'rm examples' EXIT
          cucumber '${./.}/features' -r '${./.}/steps' -c
        EOF
        chmod +x 'ruudacceptance'
      '';
      installPhase = ''
        mkdir -p "$out/bin"
        mv 'ruudacceptance' "$out/bin/ruudacceptance"
      '';
    };
in
  stdenv.mkDerivation {
    name = "acceptance";
    buildInputs = [
      cucumber
      nix
      ruudrun
      ruudacceptance
    ];
    src = ./.;
    configurePhase = ''
      echo 'Recursive Nix is currently not supported, and hence' 1>&2
      echo 'you cannot run the acceptance tests with nix-build.' 1>&2
      echo 'Run the tests with nix-shell:' 1>&2
      echo 1>&2
      echo '  $ nix-shell -A tests.acceptance --run ruudacceptance' 1>&2
      false
    '';
  }
