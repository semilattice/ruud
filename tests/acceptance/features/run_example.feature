Feature: Run example

    Running an example will build the derivation of the example and run the
    requested command. The output and exit code of the command are reported
    back to the user.

    Scenario Outline: Working example

        Given the example "<example>"
        And the command "<command>"
        When the example is run
        Then the server responds with status "200 OK"
        And the server responds with stdout "<stdout>"
        And the server responds with stderr "<stderr>"
        And the server responds with exit code "0"

        Examples:
            | example | command  | stdout        | stderr |
            | hello   | hello    | Hello, world! |        |
            | hello   | hello -t | hello, world  |        |

    Scenario: Example does not exist

        Given the example "nonexistent"
        And the command "true"
        When the example is run
        Then the server responds with status "404 Not Found"
