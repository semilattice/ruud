{ pkgs ? import ./nix/nixpkgs.nix }:
let
  ruud = import ./. {};
in
  {
    ruudrun = ruud.ruudrun.env.overrideAttrs (p: {
      nativeBuildInputs = p.nativeBuildInputs ++ [ pkgs.haskellPackages.ghcid pkgs.cabal-install ];
    });
    ruudgui = ruud.ruudgui;
    tests = {
      acceptance = ruud.tests.acceptance;
    };
  }
